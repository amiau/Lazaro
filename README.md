# Nueva estructura para Pasitos Seguros. Tiled Based Game

What we mean or what game designers mean when they're talking about tile based games is that we're using a grid of tiles to layout our game world, the graphics, the map, the movement of the sprites, everything is based on an underlying grid. There's a lot of advantages to this, it helps in organizing the data in your code, it helps in designing the graphics of your levels and you'll see how it goes together.

A lo que nos referimos o a lo que los diseñadores de juegos se refieren al hablar acerca de juegos basados en una cuadrícula es que se usa una rejilla para bosquejar el mundo, las gráficas, el mapa, el movimiento de los sprites, todo está basado en un cuadriculado. Esto tiene demasiadas ventajas, ayuda en la organización de información en el código, ayuda a diseñar los gráficos de tus niveles y verás cómo va todo junto.<br>
Esta estrategia la utilizaron algunos videojuegos al final de la década de los 70's y principios de los 80's, la cual les permitió ahorrar recursos al procesar las imágenes

# Empaquetando el código en exe (Windows)
Una vez que el proyecto ha cumplido con los funcionales fijados, éste debe pasar por un proceso de producción, el cual tiene por objetivo en hacer del código un programa ejecutable en determinada plataforma.<br>
Como se ha mencionado, Windows es la plataforma objetivo para distribuir el juego y, por tanto, se realizó un ejecutable para ésta.<br>
El proceso de empaquetado se llevó a cabo con **cx_freeze** (versión 5.1.1), una biblioteca para Python que construye el ejecutable.<br>
Además de su instalación, se requiere la elaboración de un script (setup.py) donde se especifique la inclusión de archivos y carpetas que se debe empaquetar, las bibliotecas anexas que permiten la ejecución correcta del código (dependencias), el sistema operativo donde se ejecutará, nombre, versión, autor y descripción del programa, el archivo principal del programa (main) y hasta el icono que tendrá el ejecutable.<br>
Primeramente, se debe instalar cx_freeze:
```sh
pip install cx_Freeze
```
Una vez hecho esto, se ejecuta el script de *cx_freeze* en una terminal de consola de un Windows 7.
<br>
Este archivo *setup.py* se encuentra dentro de la **carpeta Codigo** del proyecto y para obtener el exe del juego, se ejecuta:
```sh
python setup.py build
```
Guarde todo el contenido de la carpeta **build** y para jugar, simplemente haga doble click en el archivo. **main.exe**.<br>

## Agradecimientos 
* [Kenney](https://www.kenney.nl/), por su diseño [Roguelike Modern City](https://kenney.nl/assets/roguelike-modern-city) que le dio vida al mundo del juego.
* Sharm, por su aporte de las imágenes que se usaron para el personaje principal.
* Johan Brodd, por su tema [HappyWalk](https://opengameart.org/content/happy-walk) el cual se utilizó para el menú del juego.
* Marcelo Fernández, por su composición [PixelAdventures](https://opengameart.org/content/pixel-adventures) que animó la partida principal.
* edwinnington, por la pista musical [A Theme](https://opengameart.org/content/a-theme) para anunciar Game Over del juego.