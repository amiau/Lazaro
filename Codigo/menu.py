# -*- coding: utf-8 -*-

import pygame
import sys
from ScottB_menu import EVENT_CHANGE_STATE, Menu
from eztext import Input
from config import MENU_FPS, ASBESTOS


def show_menu(self):
    stay = True

    self.menu_clock = pygame.time.Clock()
    self.menu_clock.tick(MENU_FPS)
    self.screen.fill(ASBESTOS)
    self.screen.blit(self.menu_screen, [0, 0])
    self.txtbox = Input(maxlength=29, color=(52, 73, 94), x=185, y=250)

    menu = Menu(50, 50, 20, 25, 'vertical', 100, self.screen,
                 [('Empezar', 1, None),
                  ('Salir', 6, None)])

    menu1 = Menu(50, 50, 200, 25, 'horizontal', 100, self.screen,
                  [('Imagen', 2, self.button_img)])

    menu2 = Menu(50, 50, 30, 35, 'horizontal', 100, self.screen,
                  [('Ok', 3, None),
                   ('Cambiar', 4, None),
                   ('Menu', 5, None),
                   ('Salir', 6, None)])

    menu.set_center(True, True)
    menu.set_alignment('center', 'center')
    state = 0
    prev_state = 1
    rect_list = []

    while stay:
        if prev_state != state:
            pygame.event.post(pygame.event.Event(EVENT_CHANGE_STATE, key=0))
            prev_state = state

            if state in [0, 5]:
                self.screen.blit(self.menu_screen, [0, 0])
                pygame.display.flip()

        e = pygame.event.wait()

        if e.type == pygame.KEYDOWN or e.type == EVENT_CHANGE_STATE:
            if state == 0:
                rect_list, state = menu.update(e, state)

            elif state == 1:
                state = 1

                self.screen.blit(self.name_screen, [0, 0])
                menu1.set_center(False, False)
                menu1.set_position(176, 241)
                pygame.display.flip()

                while 1:
                    rect_list, state = menu1.update(e, state)

                    self.clock.tick(MENU_FPS)

                    events = pygame.event.get()
                    val = self.txtbox.update(events)
                    self.txtbox.draw(self.screen)
                    pygame.display.flip()

                    # Para guardar la cadena con el nombre del jugador (Más adelante abrir un archivo)
                    if val != None:
                        print ("Nombre del jugador: " + self.txtbox.show_value())
                        state = 2
                        break

            elif state == 2:
                state = 2
                self.screen.blit(self.ok_screen, [0, 0])
                self.screen.blit(self.button_img, [176, 241])
                self.txtbox.redraw(self.screen)

                menu2.set_center(False, False)
                menu2.set_position(130, 380)
                rect_list, state = menu2.update(e, state)
                pygame.display.flip()

            elif state == 3:
                state = 3
                print ("Iniciando juego. ¡Hola " + self.txtbox.show_value() + "! Encuentra la meta")
                stay = False

            elif state == 4:
                state = 1
                print "Cambiar nombre. Nuevo valor para nombre:"

            elif state == 5:
                self.txtbox.empty_value()
                state = 0
                print "Borrando nombre. El jugador regresó a menú inicio"

            else:
                print "Salir!"
                pygame.quit()
                sys.exit()

        if e.type == pygame.QUIT:
            print "El jugador cerró la aplicación"
            pygame.quit()
            sys.exit()

        pygame.display.update(rect_list)