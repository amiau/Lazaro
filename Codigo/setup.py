import sys
from cx_Freeze import setup, Executable

buildOptions = dict(include_files =
['maps/', 'images/', 'config.py', 'eztext.py', 'kid.py', 'map.py', 'menu.py', 'score.py', 'ScottB_menu.py', 'spritesheet.py'],
packages=['pygame','pytmx','six']
)

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name='Pasitos seguros',
    version='1.0.0',
    description='Juego serio para educacion vial',
    author='Rebeca Roldan',
    author_email='rmroldanl001@alumni.uaemex.mx',
    # options = {'build_exe': {'includes':includes,'excludes':excludes,'packages':packages,'include_files':includefiles}},
    #executables = [Executable('janitor.py')]
    options=dict(build_exe = buildOptions),
    executables = [
    Executable(
        'main.py',
        base=base,
        icon='tl.ico' )
    ]
)
