import pygame
import pytmx
from pytmx.util_pygame import load_pygame
from config import *

class TiledMap:
    def __init__(self, filename):
        # Si necesitamos permitir duplicados, lo cual no recomiendo... allow_duplicate_names=True

        tiled = load_pygame(filename, pixelalpha= True, allow_duplicate_names=True)
        self.width = tiled.width * tiled.tilewidth
        self.height = tiled.height * tiled.tileheight
        self.tmxdata = tiled

    def render(self, surface):
        ti = self.tmxdata.get_tile_image_by_gid
        for layer in self.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledTileLayer):
                for x, y, gid, in layer:
                    tile = ti(gid)
                    if tile:
                        surface.blit(tile, (x * self.tmxdata.tilewidth, y * self.tmxdata.tileheight))

    def make_map(self):
        tmp_surface = pygame.Surface((self.width, self.height))
        self.render(tmp_surface)
        return tmp_surface

class Camera:
    def __init__(self, width, height):
        self.camera = pygame.Rect(0, 0, width, height)
        self.width = width
        self.height = height

    def apply(self, entity):
        return entity.rect.move(self.camera.topleft)

    def apply_rect(self, rect):
        return rect.move(self.camera.topleft)

    def update(self, player_sprite):
        x = -player_sprite.rect.centerx + int(WIDTH/2)
        y = -player_sprite.rect.centery + int(HEIGHT/2)

        x = min(0, x)
        y = min(0, y)
        x = max(-(self.width - WIDTH), x)
        y = max(-(self.height - HEIGHT), y)
        self.camera = pygame.Rect(x, y, self.width, self.height)