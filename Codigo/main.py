# -*- coding: utf-8 -*-

import pygame
import sys
from config import *
from os import path
from kid import Player
from map import TiledMap, Camera, pytmx
from score import seek_groups
from menu import show_menu


class KidsGame:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption('Pasitos Seguros')
        self.clock = pygame.time.Clock()
        self.load_data()


    def load_data(self):
        main_folder = path.dirname('__file__')
        img_folder = path.join(main_folder, "images")
        self.map_folder = path.join(main_folder, "maps")
        self.menu_screen = pygame.image.load(path.join(img_folder, MENU_0))
        self.name_screen = pygame.image.load(path.join(img_folder, MENU_1))
        self.button_img = pygame.image.load(path.join(img_folder, BUTTON))
        self.ok_screen = pygame.image.load(path.join(img_folder, MENU_2))
        self.start_screen = pygame.image.load(path.join(img_folder, START))
        self.alpha_screen = pygame.Surface(self.screen.get_size()).convert_alpha()
        self.alpha_screen.fill((0, 0, 0, 180))
        self.g_over_screen = pygame.image.load(path.join(img_folder, G_OVER))
        self.k_wagen1 = pygame.image.load(path.join(img_folder, WAGEN1))
        self.k_wagen2 = pygame.image.load(path.join(img_folder, WAGEN2))
        self.today_tip = pygame.image.load(path.join(img_folder, T_TIP))
        pygame.mixer.music.load('sound/HappyWalk.ogg')
        pygame.mixer.music.play(-1, 0.0)


    def new(self):
        self.all_game_sprites = pygame.sprite.Group()
        self.level1_map = TiledMap(path.join(self.map_folder, "level_1.tmx"))
        self.map_surface = self.level1_map.make_map()
        self.map_rect = self.map_surface.get_rect()
        self.player = Player(self)
        # self.all_game_sprites.add(self.player)
        self.player.ini_pos()
        self.camera = Camera(self.level1_map.width, self.level1_map.height)
        self.zebra_list = []
        self.clean_id_list = []
        self.penalty_list = []
        self.count_penalties = []
        self.game_font = pygame.font.Font(None, 35)
        self.high_score = self.game_font.render('High score: 1390', True, CLOUDS, CONCRETE)
        self.game_over = False

    def run(self):
        self.playing = True
        while self.playing:
            self.clock.tick(FPS) / 1000.0
            self.text_score = self.game_font.render("Score: " + str(self.player.score), True, CLOUDS, CONCRETE)
            self.events()
            self.update()
            self.render()

    def bye(self):
        pygame.quit()
        sys.exit()

    def update(self):
        self.all_game_sprites.update()
        self.camera.update(self.player)

        for layer in self.level1_map.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledObjectGroup):
                if layer.name == "obstacles":
                    for obj in layer:
                        if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                            self.player.objects_collision()
                            # print "You hit a wall!"
                            break

        for obj in self.level1_map.tmxdata.objects:
            if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                self.zebra_list.append(obj.id)
                self.clean_id_list = list(set(self.zebra_list))
                # print self.clean_id_list
                # print len(self.clean_id_list)
                self.player.score = seek_groups(self.clean_id_list)
                if obj.name == "meta":
                    self.player.score = self.player.score + 1000
                    # print "Final score: ", self.player.score
                    self.game_over = True
                    break

        for layer in self.level1_map.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledObjectGroup):
                if layer.name == "penalties":
                    for obj in layer:
                        if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                            self.penalty_list.append(obj.id)
                            self.count_penalties = list(set(self.penalty_list))
                            # print self.count_penalties
                            if len(self.count_penalties) == 10:
                                # print "Game Over :("
                                # print "penalty! id: " + str(obj.id)
                                # print "Penalizaciones: " + str(len(self.count_penalties))
                                self.player.accident = True
                                self.game_over = True
                                break

        if self.game_over == True:
            self.playing = False

    def render(self):
        self.screen.blit(self.map_surface, self.camera.apply_rect(self.map_rect))
        for every_sprite in self.all_game_sprites:
            self.screen.blit(every_sprite.image, self.camera.apply(every_sprite))
        self.screen.blit(self.text_score, [10, 0])
        self.screen.blit(self.high_score, [570, 0])
        pygame.display.flip()

    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.bye()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT and self.player.vel_x < 0:
                    self.player.vel_x = 0
                elif event.key == pygame.K_RIGHT and self.player.vel_x > 0:
                    self.player.vel_x = 0
                elif event.key == pygame.K_UP and self.player.vel_y < 0:
                    self.player.vel_y = 0
                elif event.key == pygame.K_DOWN and self.player.vel_y > 0:
                    self.player.vel_y = 0

    def show_start_screen(self):
        self.screen.blit(self.start_screen, [0, 0])
        pygame.display.flip()
        self.waiting_for_user()

    def game_over_screen(self):
        self.screen.blit(self.alpha_screen, [0, 0])
        self.screen.blit(self.g_over_screen, [0, 0])
        final_score = self.game_font.render(str(self.player.score), True, CLOUDS)
        self.screen.blit(final_score, [507, 282])
        pygame.display.flip()
        self.waiting_for_user()

    def accident_screen(self):
        pygame.mixer.music.load('sound/ATheme.ogg')
        pygame.mixer.music.play(-1, 0.0)
        self.screen.fill(MIDNIGHT_BLUE)
        # Animar el gif de la ambulancia
        self.screen.blit(self.k_wagen1, [202, 73])
        self.screen.blit(self.k_wagen2, [202, 73])
        # Preparar algunos consejos viales
        self.screen.blit(self.today_tip, [0, 0])
        warning_text = self.game_font.render("Pulsa cualquier tecla", True, CLOUDS)
        self.screen.blit(warning_text, [257, 500])
        pygame.display.flip()
        self.waiting_for_user()

    def waiting_for_user(self):
        pygame.event.wait()
        waiting = True
        while waiting:
            self.clock.tick(MENU_FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    waiting = False
                    self.bye()
                if event.type == pygame.KEYUP:
                    waiting = False


game = KidsGame()
show_menu(game)
game.show_start_screen()

while True:
    pygame.mixer.music.set_volume(0.3)
    pygame.mixer.music.load('sound/PixelAdventures.ogg')
    pygame.mixer.music.play(-1, 0.0)
    game.new()
    game.run()
    if game.player.accident == False:
        game.game_over_screen()
    if game.player.accident == True:
        pygame.mixer.music.stop()
        game.accident_screen()
